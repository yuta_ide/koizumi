#!/usr/local/bin/python
# -*- coding: utf-8 -*-

import sys
import io
import json
from bottle import route, run, request, response, static_file
import requests
from anal import extractKeyword, createWordMatrix, analMatrix
import pandas as pd
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import pyplot

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')

# routeデコレーター
# これを使用してURLのPathと関数をマッピングする。
@route('/hello')
def hello():
  return "Hellfssso Worlds!f"

@route('/anal')
def anal():
    r = requests.get('http://52.69.5.48:3000/api/v1/comments').json()
    data = r["data"]
    comments = []
    for i in range(len(data)):
        tmp = []
        d = data[i]
        tmp.append(extractKeyword(str(d["content"])))
        comments.append(tmp)
    print(createWordMatrix(comments))
    wm = createWordMatrix(comments)
    print(wm)
    analMatrix(wm)
    return str(analMatrix(wm))

@route('/<file_path:path>')
def static(file_path):
    return static_file(file_path, root='./')
# ビルトインの開発用サーバーの起動
# ここでは、debugとreloaderを有効にしているc
run(host='0.0.0.0', port=8080, debug=True, reloader=True)
