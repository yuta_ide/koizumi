# KNOWLEDGE
## ライブラリ
**何をインストールして, どんな働きをするか明記すること.**

## 開発工程
### srcからdistにbuildできるように
  * gulpのインストール
    - `npm install gulp -g`
      + gulpはグローバルとローカルの両方にインストールする必要がある.
    - `npm install gulp -D`
    - `gulp -v`
      + CLIとlocalと表示されたらglobalにもlocalにもインストールされていることが示される.
  * rimrafのインストール
    - ディレクトリ内をクリーンできる.
    - `npm install rimraf -D `

### eslintが働くように
  (http://qiita.com/ynunokawa/items/5471ff84c83104450ecb)
  *  eslintのインストール
    - `npm install eslint -D`
  *  eslint-config-airbnbのインストール
    - `npm install eslint-config-airbnb -D`
    - airbnbのスタイルガイドをlinterとして設定できるようになる.
  *  gulp-plumberのインストール
    - `npm install gulp-plumber -D`
    - エラー抑制のための gulp プラグイン.
  *  node-notifierのインストール
    - `npm install node-notifier -D`
    - 通知送信のための gulp プラグイン
  * `.eslintrc`を設置し, linterの設定を記述
  * `gulpfile.js`でgulp-eslintが.eslintrcを解釈するメソッドを提供するので, 使う.

### ES6が使えるように-webpackを用いたトランスパイル-
  * webpackのインストール
    - ビルドとトランスパイルはwebpackで行う
    - `npm install webpack -D  `
  * gulp-webpackのインストール
    - gulpからwebpackをよびだせる
    - `npm install gulp-webpack -D`
  * babel系をインストール
    (http://qiita.com/kamijin_fanta/items/e8e5fc750b563152bbcf)
    - babelパッケージは使えない. (バージョン6でいろいろなものに分離された. )
    - `npm install -D webpack babel-core babel-loader babel-preset-es2015`
  * `webpack.config.js`のmoduleのloaderとして
    ```
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader'
    }]
    ```
    を指定することで, babel-loaderがbabelを呼び出し, 実際にトランスパイルをしてくれる.

### vue.jsを使って画面に表示させる.
  vueは1系を使おう.
  * vue.jsのインストール
    - `npm install -S vue`
    - 1.0.24
  * vue-routerのインストール
    - `npm install -S vue-router`
    - 0.7.13
  * vuexのインストール
    - `npm install -S vuex`
    - 0.6.3
  * vue-resourceのインストール
    - `npm install -S vue-resource`
    - 0.7.2
  * vuex-router-syncのインストール
    - `npm install -S vuex-router-sync`
    - 2.0.0
  * vue-loaderのインストール
    - `npm install -S vue-loader`
    - 8.5.3
    - `vue-loader`は9系を使わないこと.
    - 9系では`vue-template-compiler`が2系になってしまっている.

### admin-lteの導入
#### 依存ライブラリ
  * bootstrap
    - `npm install -S bootstrap`
    - `main.js`にて`require('../node_modules/bootstrap/dist/css/bootstrap.css');`することでcssを読み込める.
    - css, js, ttfなどの各種loaderをinstallすること.
    - webpackのloaderの設定
      + loaderとして次の設定が必要. ```
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.vue$/,
        loader: 'vue'
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file'
      },
      {
        test: /\.(woff|woff2)$/,
        loader: 'url?prefix=font/&limit=5000'
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&mimetype=application/octet-stream',
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&mimetype=image/svg+xml'
      }
        ```
  * jquery
    - `npm install -S jquery`
    - webpackのpluginでglobalに差し込む.
      + ```
      new webpack.ProvidePlugin({
          jQuery: "jquery",
          $: "jquery"
      })
      ```
    - `main.js`で`import $ from jquery`
  * admin-lte
    - `npm install -S admin-lte`
    - `main.js`にて`require('../node_modules/admin-lte/dist/css/AdminLTE.min.css');`
    - 実はbootstrapはrequireしなくても良い.

  * bootstrap-datepicker
    - `npm i -S bootstrap-datepicker`
　　

## Tips

## Question
