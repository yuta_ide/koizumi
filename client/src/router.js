import Vue from 'vue';
import VueRouter from 'vue-router';
import A from './components/A.vue';
import B from './components/B.vue';
import C from './components/C.vue';
import TakaminoKenbutsuMan from './components/TakaminoKenbutsuMan.vue';

Vue.use(VueRouter);
const router = new VueRouter();

router.map({
  '/A': {
    component: A
  },
  '/B': {
    component: B
  },
  '/C': {
    component: C
  },
  '/TakaminoKenbutsuMan': {
    component: TakaminoKenbutsuMan
  }
});

export default router;
