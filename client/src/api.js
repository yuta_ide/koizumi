import Vue from 'vue';
import Resource from 'vue-resource';

Vue.use(Resource);

Vue.http.headers.common['Content-Type'] = 'application/x-www-form-urlencoded;application/json;charset=utf-8';
Vue.http.headers.common['Access-Control-Allow-Origin'] = 'http://52.69.5.48:3000'

const API_HOST = 'http://52.69.5.48:3000/api/v1';
// const API_HOST = 'http://127.0.0.1:3000/api/v1';

const resource = function(url, options) {
  return Vue.resource(url, null, null, { emulateJSON: true });
};

export default class API {
  static postForm(user_id, object, feeling, content, latitude, longitude, page, is_micro){
    // curl -X POST http://52.69.5.48:3000/api/v1/comments -d "comment[user_id]=4" -d "comment[name]=パン" -d "comment[content]=電源、充電"
    console.info("[API] postForm is called");
    console.log(`content: ${content}`)
    return (new Promise((resolve, reject) => {
      resource(`${API_HOST}/comments`)
      .save({'comment[user_id]': user_id, 'comment[name]' : object, 'comment[feeling]': feeling, 'comment[content]': content, 'comment[latitude]' : latitude, 'comment[longitude]': longitude, 'comment[page]' : page, 'comment[is_micro]' : is_micro})//stateからidを取得しよう
      .then(() => {
        console.log("[API]postComment成功")
        resolve()
      })
      .catch((reason) => {
        console.log("[API]postComment失敗 reason:" + reason)
        console.log(reason)
      })
    }))
    console.log(`promiseDone`)
  }

  static getComment(){
    console.info("[API] getComment is called");
    return (new Promise((resolve, reject) => {
      resource(`${API_HOST}/comments`).get(null, {})
      .then((data) => {
        console.log("[API]getComment成功 data.data:");
        console.log(data.data);
        resolve(data.data)
      }).catch((data) => {
        console.log("[API]getComment失敗 reason:" + reason)
        reject(data)
      })
    }))
  }
};

/*
curl 'http://52.193.173.211:3000/api/v1/comments' \
-XPOST \
-H 'Content-Type: application/x-www-form-urlencoded' \
-H 'Origin: file://' \
-H 'Accept: application/json, text/plain' \
-H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12) AppleWebKit/602.1.50 (KHTML, like Gecko) Version/10.0 Safari/602.1.50' \
--data 'comment%5Buser_id%5D=7&comment%5Bname%5D=ji&comment%5Bfeeling%5D=2&comment%5Bcontent%5D=jig&comment%5Blatitude%5D=36.444871263832525&comment%5Blongitude%5D=136.50832707287046&comment%5Bpage%5D=&comment%5Bis_micro%5D=0'
*/

/*
curl 'http://52.193.173.211:3000/api/v1/comments?comment%5Buser_id%5D=0&comment%5Bname%5D=%E3%83%A9%E3%83%BC%E3%83%A1%E3%83%B3&comment%5Bfeeling%5D=0&comment%5Bcontent%5D=%E3%82%AB%E3%83%AC%E3%83%BC%E3%81%AF%E3%83%91%E3%83%B3%E5%B1%8B%E3%81%95%E3%82%93%E3%81%A7%E8%B2%B7%E3%81%88%E3%82%8B%E3%80%82%E5%90%9B%E3%81%AE%E5%90%8D%E3%81%AF%E3%80%82&comment%5Blatitude%5D=&comment%5Blongitude%5D=&comment%5Bpage%5D=&comment%5Bis_micro%5D=0' \
-XPOST \
-H 'Content-Type: application/x-www-form-urlencoded' \
-H 'Referer: http://52.193.173.211/' \
-H 'Accept: application/json, text/plain' \
-H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12) AppleWebKit/602.1.50 (KHTML, like Gecko) Version/10.0 Safari/602.1.50' \
-H 'Origin: http://52.193.173.211'
*/
