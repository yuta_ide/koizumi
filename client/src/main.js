import Vue from 'vue';
import { sync } from 'vuex-router-sync';
import $ from 'jquery';
import 'bootstrap';
import 'admin-lte';
import Root from './components/Root.vue';
import store from './vuex/store';
import router from './router';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'admin-lte';
import 'admin-lte/dist/css/AdminLTE.min.css';
import 'admin-lte/dist/css/skins/skin-blue.css';

sync(store, router);
router.start(Root, '#app');
