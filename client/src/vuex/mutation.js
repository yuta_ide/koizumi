import API from '../api.js';

export default {
  RENDER_MAP(state){
    state.map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: 35.681298,lng: 139.766247},
      streetViewControl: false,
      zoom: 13
    });
  }
}
