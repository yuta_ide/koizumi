class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :name
      t.string :content
      t.float :longitude
      t.float :latitude
      t.integer :feeling
      t.integer :page
      t.boolean :is_micro
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
