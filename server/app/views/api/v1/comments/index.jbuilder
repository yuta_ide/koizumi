json.set! :data do
  json.array!(@comments) do |comment|
    json.merge! comment.attributes
  end
end
