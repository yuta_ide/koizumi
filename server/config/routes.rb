Rails.application.routes.draw do
  resources :comments
  resources :users
  namespace :api, { format: 'json' }  do
    namespace :v1 do
      resources :comments
    end
  end
end
